package Lesson5;

public class Employee {

    int id;
    String surname;
    int age;
    double salary;
    String department;

    Employee(int setId, String name, int years, double money, String dep) {
        id = setId;
        surname = name;
        age = years;
        salary = money;
        department = dep;
    }

    public void addSalary() {
        salary = salary * 2;
    }
}

class TestEmployee {
    public static void main(String[] args) {
        Employee empl1 = new Employee(1, "Gavr", 38, 3100, "IT");
        Employee empl2 = new Employee(2, "Tukh", 36, 1500, "IT");

        empl1.addSalary();
        empl2.addSalary();

        System.out.println("empl1: " + empl1.salary);
        System.out.println("empl2: " + empl2.salary);
    }
}
