package Lesson5;

public class BankAccount {

    double money = 0;

    public static void main(String [] args) {
        BankAccount account = new BankAccount();
        account.increase(100.23);
        account.decrease(10);
        account.info();
    }

    public void increase(double summ) {
        money += summ;
    }

    public void decrease(double summ) {
        money -= summ;
    }

    public void info()
    {
        System.out.println("Summa: " + money);
    }
}
