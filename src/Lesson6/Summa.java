package Lesson6;

public class Summa {
    int sum() {
        return 0;
    }

    int sum(int a1) {
        return sum(a1, 0);
    }

    int sum(int a1, int a2) {
        return sum(a1, a2, 0);
    }

    int sum(int a1, int a2, int a3) {
        return sum(a1, a2, a3, 0);
    }

    int sum(int a1, int a2, int a3, int a4)
    {
        return a1 + a2 + a3 + a4;
    }
}
