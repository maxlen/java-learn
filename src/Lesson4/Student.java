package Lesson4;

public class Student {
    int id;
    String name;
    String lastname;
    int year;
    int markMath;
    int markEconom;
    int markLang;
}

class StudentTest {
    public static void main(String [] args) {
        Student student1 = new Student();
        student1.id = 1;
        student1.name = "Maxim";
        student1.lastname = "Gavrilenko";
        student1.year = 2001;
        student1.markMath = 12;
        student1.markEconom = 4;
        student1.markLang = 10;
        double student1Sred = (student1.markMath + student1.markEconom + student1.markLang)/3;

        Student student2 = new Student();
        student2.id = 2;
        student2.name = "Vasya";
        student2.lastname = "Evdokimov";
        student2.year = 1980;
        student2.markMath = 10;
        student2.markEconom = 11;
        student2.markLang = 4;
        double student2Sred = (student2.markMath + student2.markEconom + student2.markLang)/3;

        Student student3 = new Student();
        student3.id = 3;
        student3.name = "Alina";
        student3.lastname = "Perova";
        student3.year = 2010;
        student3.markMath = 12;
        student3.markEconom = 11;
        student3.markLang = 10;
        double student3Sred = (student3.markMath + student3.markEconom + student3.markLang)/3;

        System.out.print(student1.name);
        System.out.print(" ");
        System.out.print(student1.lastname);
        System.out.print(":   ");
        System.out.println(student1Sred);

        System.out.print(student2.name);
        System.out.print(" ");
        System.out.print(student2.lastname);
        System.out.print(":   ");
        System.out.println(student2Sred);

        System.out.print(student3.name);
        System.out.print(" ");
        System.out.print(student3.lastname);
        System.out.print(":   ");
        System.out.println(student3Sred);
    }
}