package Lesson4;

public class BankAccount {
    int id;
    String name;
    double balance;
}

class BankAccountTest
{
    public static void main(String [] args) {
        BankAccount MyAccount = new BankAccount();
        BankAccount YourAccount = new BankAccount();
        BankAccount HisAccount = new BankAccount();

        MyAccount.id = 1;
        MyAccount.name = "Maxim";
        MyAccount.balance = 12.35;

        YourAccount.id = 2;
        YourAccount.name = "Ania";
        YourAccount.balance = 10.00;

        HisAccount.id = 3;
        HisAccount.name = "Ivan";
        HisAccount.balance = 34.24;

        System.out.println(MyAccount);

    }
}