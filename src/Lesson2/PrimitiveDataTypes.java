package Lesson2;

public class PrimitiveDataTypes {
    public static void main(String [] args ) {
        byte b1 = 10;
        byte b2 = 20;
        byte b3 = 100;

        short s1 = 10;
        short s2 = -10;
        short s3 = 510;

        int i1 = 500;

        long l1 = 1000;
        long l2 = 10000000000L;

        float f1 = 3.14F;

        double d1 = 2.34;

        char c1 = 'q';
        char c2 = 'Q';
        char c3 = '7';

        boolean bool1 = true;
        boolean bool2 = false;

        System.out.println(bool1);
    }
}
